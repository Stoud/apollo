package me.stoud.apollo.indexer.item;

import java.io.File;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import me.stoud.apollo.exception.ImportNotResolvableException;
import me.stoud.apollo.indexer.Index;

public class MethodIndex extends Index{
	private final static Logger LOGGER = Logger.getLogger(MethodIndex.class.getName());
	private String methodName;
	private String methodDescriptor;//TODO Create some sort of MethodIdentifier class
	private String body;			//Currently this might be somewhat annoying to implement
	private ClassIndex classIndex;
	
	
	public MethodIndex(String className, String packageName, String methodName, String body, String methodDescriptor, ClassIndex ci){
		super(className,packageName);
		this.methodName = methodName;
		this.body = body;
		this.classIndex = ci;
	}

	@Override
	public void index() {//TODO CREATE VARIABLE CLASS	
		Scanner bodyReader = new Scanner(body);
		StringTokenizer st = new StringTokenizer(body,";");
		while(st.hasMoreTokens()){
			StringTokenizer line = new StringTokenizer(st.nextToken());
			while(line.hasMoreTokens()){
				String token = line.nextToken();
				try {
					File file = classIndex.getImportFile(token);
				} catch (ImportNotResolvableException expected) {
					//The this just means the token is not a valid class. This could occur if the token is a variable name
				}
				
			}
		}
	}

	@Override
	public String toString() {
		return "Method index of " + methodName + " method with descriptor " + methodDescriptor + " in class " + this.packageName + this.className + " - " + this.hashCode();
	}
}
