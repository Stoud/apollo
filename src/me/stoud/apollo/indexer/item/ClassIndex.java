package me.stoud.apollo.indexer.item;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import me.stoud.apollo.exception.ImportNotResolvableException;
import me.stoud.apollo.indexer.Index;

public class ClassIndex extends Index{
	private final static Logger LOGGER = Logger.getLogger(ClassIndex.class.getName());
	private String body;
	private Map<String,File> imports;

	public ClassIndex(String name, String packageName){
		super(name,packageName);
		imports = new HashMap<String,File>();
	}

	public void addImport(String className, String packageName){
		imports.put(className, new File(packageName));
	}

	public File getImportFile(String className) throws ImportNotResolvableException{ //TODO SCRAP, add package name
		if(!imports.containsKey(className)){
			throw new ImportNotResolvableException(className);
		}
		File importToGet = imports.get(className);
		if(importToGet.exists()){
			return imports.get(className);
		}
		throw new ImportNotResolvableException(className);
	}

	@Override
	public void index() {
		StringTokenizer reader = new StringTokenizer(body);
		resolveImports(reader);
		
	}
	
	
	private void resolveImports(StringTokenizer reader){ 
		while(reader.hasMoreTokens()){
			String line = reader.nextToken();
			if(line.startsWith("package")){
			}else if(line.startsWith("import")){
				StringTokenizer importReader = new StringTokenizer(line.substring(line.indexOf(" ")), ".");
				String file = "";
				while(importReader.countTokens() > 1){
					file += importReader.nextToken() + "/";
				}
				addImport(importReader.nextToken(),file);
			}else{
				break;
			}
		}
	}

	@Override
	public String toString() {
		return "ClassIndex index of " + this.packageName + this.className + " - " + this.hashCode();
	}

}
