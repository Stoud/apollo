package me.stoud.apollo.indexer.item;

import java.util.Arrays;

import me.stoud.apollo.indexer.Index;

public class ConstructorIndex extends Index{
	private String[] params;
	
	public ConstructorIndex(String name, String packageName, String[] paramaters) {
		super(name, packageName);
		this.params = paramaters;
	}
	
	
	@Override
	public void index() {
		//TODO
	}

	@Override
	public String toString() {
		return "Constructor index of " + this.className + " with paramaters " + Arrays.toString(params) + " in class " + this.packageName + this.className + " - " + this.hashCode();
	}
}
