package me.stoud.apollo.indexer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Index {
	protected List<Index> parents;
	protected List<Index> children;
	protected String className;
	protected String packageName;

	private String body;
	private final static Logger LOGGER = Logger.getLogger(Index.class.getName());
	
	protected Index(String name, String packageName){
		this.parents = new ArrayList<Index>();
		this.children = new ArrayList<Index>();
		this.className = name;
		this.packageName = packageName;
		LOGGER.log(Level.FINER, "Index with name: " + name + " and package name " + packageName + "created");
	}
	/**
	 * Used to notate that this message has a parent of parent
	 * Registers the parent as a parent, and the child as a child
	 * @param parent
	 */
	public void registerParent(Index parent){
		parents.add(parent);
		parent.children.add(this);
	}
	
	public List<Index> getParents(){
		return parents;
	}
	
	public List<Index> getChildren(){
		return children;
	}
	
	/**
	 * This method indexes every child it has. 
	 * It should be called no more than one time.
	 * If it is called greater than once, lots of time will be wasted, and double results could occur.
	 * At the start of being called, it should call Apollo.markAsIndexed(this)
	 */
	public abstract void index();
	
	
	@Override
	/**
	 * Overrides toString in object
	 */
	public abstract String toString();

}
