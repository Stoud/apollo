package me.stoud.apollo.indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.stoud.apollo.exception.MainClassNotFoundException;

public class Indexer {
	private final static Logger LOGGER = Logger.getLogger(Indexer.class.getName());
	
	private File sourceDirectory;
	private String classForIndexing;
	
	/**
	 * This object is purely for finding the classes to start and actually beginning the indexing of the source.
	 * @param sourceDir
	 * @param classToBeginIndexing
	 */
	public Indexer(File sourceDir, String classToBeginIndexing){
		this.sourceDirectory = sourceDir;
		this.classForIndexing = classToBeginIndexing;		
	}

	/**
	 * This method starts the indexing of everything. It either uses a provided class or defaults to a main class.
	 * @throws FileNotFoundException
	 * @throws MainClassNotFoundException
	 */
	public void index() throws FileNotFoundException, MainClassNotFoundException{
		if(classForIndexing != null){
			index(getClassNameFromImport(classForIndexing), getPackageNameFromImport(classForIndexing));
		}else{
			String[] mainClassNameAndPackage =  findMain();
			index(mainClassNameAndPackage[0],mainClassNameAndPackage[1]);
		}
	}
	
	private String getClassNameFromImport(String importIn){
		return importIn.split(".")[importIn.split(".").length - 1];
	}
	
	private String getPackageNameFromImport(String importIn){
		String name = "";
		String[] packagesArray = importIn.split(".");
		for(int i = 0; i < packagesArray.length - 1; i++){
			name += packagesArray[i] + ".";
		}
		return name;
	}
	
	
	/**
	 * Finds the main by checking the manifest and parsing the data inside there.v
	 * @return String[] in the form of Class name, Package name
	 * @throws MainClassNotFoundException
	 * @throws FileNotFoundException
	 */
	private String[] findMain() throws MainClassNotFoundException, FileNotFoundException {
		File manifest = new File(sourceDirectory.getPath() + "/META-INF/MANIFEST.MF");
		if(manifest.exists()){
			Scanner reader = new Scanner(manifest);
			while(reader.hasNextLine()){
				String line = reader.nextLine();
				if(line.toLowerCase().contains("main-class:")){
					String mainClass = line.substring(line.toLowerCase().indexOf("main-class: "));//TODO Account for no space in main-Class line
					LOGGER.log(Level.FINEST, "Main class found to be: " + mainClass);
					return new String[]{getClassNameFromImport(line), getPackageNameFromImport(line)};
				}
			}
			reader.close();
		}
		throw new MainClassNotFoundException();

	}
	
	
	private void index(String className, String packageName){
		LOGGER.log(Level.INFO, "Beginning to index source class: " + className + " in package: " + packageName);

		//TODO 
	}
	

}
