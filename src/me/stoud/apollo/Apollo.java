package me.stoud.apollo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import me.stoud.apollo.exception.MainClassNotFoundException;
import me.stoud.apollo.indexer.Index;
import me.stoud.apollo.indexer.Indexer;
import me.stoud.apollo.parser.ArgsParser;

public class Apollo {
	private final static Logger LOGGER = Logger.getLogger(Apollo.class.getName());
	private static Set<Index> indexedItems;
	
	
	public static void main(String[] args) throws FileNotFoundException, MainClassNotFoundException {
		indexedItems = new HashSet<Index>();
		//Get and parse arguments
		ArgsParser parser = new ArgsParser(args);
		String sourceLocation = parser.getValue("dir", "."); //Get the directory to index code from, defaults to the current directory.
		File sourceDirectory = new File(sourceLocation);
		if(!sourceDirectory.exists()){
			throw new RuntimeException("Unable to find directory of source");
		}
		
		String classToBeginIndexing = parser.getValue("class",null);//Get the start class
		//End parsing arguments.

		
		//Create an indexer to index everything. By default it looks for the MainClass, but the class to index by can be changed using the class argument
		Indexer indexer = new Indexer(sourceDirectory, classToBeginIndexing);
		indexer.index();
		
	}
	
	public static void markAsIndexed(Index index){
		indexedItems.add(index);
	}
	
	public static boolean hasBeenIndexed(Index index){
		return indexedItems.contains(index);
	}



}
