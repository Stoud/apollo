package me.stoud.apollo.exception;

public class ImportNotResolvableException extends ApolloException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8137109297665690337L;
	
	public ImportNotResolvableException(String msg){
		super(msg);
	}
}
