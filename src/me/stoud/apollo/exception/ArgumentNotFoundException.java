package me.stoud.apollo.exception;

import me.stoud.apollo.Apollo;

public class ArgumentNotFoundException extends ApolloException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8291374195233694235L;

	public ArgumentNotFoundException(String message){
		super(message);
	}
}
