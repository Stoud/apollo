package me.stoud.apollo.exception;

public abstract class ApolloException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ApolloException(){
		super();
	}
	public ApolloException(String message){
		super(message);
	}
}
