package me.stoud.apollo.exception;

public class MainClassNotFoundException extends ApolloException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3499715674844249201L;
	
	public MainClassNotFoundException(){
		super();	
	}
}
