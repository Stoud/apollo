package me.stoud.apollo.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.stoud.apollo.exception.ArgumentNotFoundException;

public class ArgsParser {
	private final static Logger LOGGER = Logger.getLogger(ArgsParser.class.getName());
	
	/**
	 * Map that contains arguments with values.
	 * For example -path fillerTextHere would be mapped as (path, fillerTextHere)
	 * If the argument has no sub-argument(for -path would be fillerTextHere), it is mapped as (path, null)
	 */
	private Map<String, String> args;
	
	
	public ArgsParser(String[] args){
		this.args = new HashMap<String, String>(); //Instantiate args
		
		for(int i = 0; i < args.length; i++){ //Loop through args
			String arg = args[i];
			
			if(arg.startsWith("-")){ //If it is an argument
				if(!(args[i+1].startsWith("-"))){ //If it has a sub-argument
					put(arg.substring(1), args[i+1]); //Put the argument with the sub-argument
					i++; //Uneeded, but notes that the next element was processed
				}else{
					put(arg, null); //It has no sub-argument, so store it as (arg, null)
				}
				
			}
		}
	}

	/**
	 * Notes that a pair of arguments was detected and adds it to the map
	 * @param arg
	 * @param subArg
	 */
	private void put(String arg, String subArg) {
		LOGGER.log(Level.INFO, "Detected argument \"" + arg + "\" and sub-argument \"" +subArg + "\"");
		this.args.put(arg,subArg);
		
	}

	/**
	 * Returns the value of a given argument, it throws an ArgumentNotFoundException and returns the 
	 * @param arg
	 * @param defaultValue
	 * @return
	 */
	public String getValue(String arg, String defaultValue){
		try{
			if(args.get(arg) == null){
				throw new ArgumentNotFoundException(arg + " not found in arguments but was requested.");
			}
			return args.get(arg);
		}catch(ArgumentNotFoundException e){
			e.printStackTrace();
			return defaultValue;
		}
	}

	public String getValue(String arg){
		return getValue(arg, null);
	}

}
